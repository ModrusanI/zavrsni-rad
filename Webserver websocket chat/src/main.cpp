#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WebSocketsServer.h>

WebServer server;
WebSocketsServer webSocket = WebSocketsServer(81);

uint8_t pin_led = 2;
const char *ssid = "ivan";
//char *password = "";

void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length)
{
  switch (type)
  {
  case WStype_DISCONNECTED:
    Serial.printf("[WSc] Disconnected!\n");
    break;
  case WStype_CONNECTED:
    Serial.printf("[WSc] Connected to url: %s\n", payload);  
    break;

  case WStype_TEXT:
    Serial.printf("[WSc] Message: %s\n", payload);

    webSocket.broadcastTXT(payload, length);

    digitalWrite(pin_led, HIGH);
    delay(50);
    digitalWrite(pin_led, LOW);
    break;

  case WStype_BIN:
    Serial.printf("[WSc] get binary length: %u\n", length);
    break;
  case WStype_PING:
    break;
  case WStype_PONG:
    break;

  case WStype_FRAGMENT_TEXT_START:
    break;

  case WStype_FRAGMENT_BIN_START:
    break;

  case WStype_FRAGMENT:
    break;

  case WStype_FRAGMENT_FIN:
    break;

  case WStype_ERROR:
    break;
  }
}

char webpage[] PROGMEM = R"=====(
<html>
<head>
  <script>
    var Socket;
    function init() {
      Socket = new WebSocket('ws://' + window.location.hostname + ':81/');
      Socket.onmessage = function(event){
        document.getElementById("chatText").value += event.data + "\n";
        document.getElementById("chatText").scrollTop = document.getElementById("chatText").scrollHeight 
      }
    }
    function sendText(){
      Socket.send(document.getElementById("txBar").value);
      document.getElementById("txBar").value = "";
    }
  
  </script>

  <style>
  html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}
  textarea { height: 50vh; width: 80vw; text-align:justify; margin-bottom: 15px} 
  .inputText {width: 80vw, height: 20px}
  </style>
</head>
<body onload="javascript:init()">
  <div>
    <textarea id="chatText"></textarea>
  </div>
  <div>
    <input class="inputText" type="text" id="txBar" onkeydown="if(event.keyCode == 13) sendText();" />
  </div> 
</body>
</html>
)=====";

void setup()
{
  pinMode(pin_led, OUTPUT);
  WiFi.begin(ssid);
  Serial.begin(115200);
    // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  while(WiFi.status()!=WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  server.on("/",[](){
    server.send_P(200, "text/html", webpage);  
  });
  server.begin();
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop()
{
  webSocket.loop();
  server.handleClient();
}
